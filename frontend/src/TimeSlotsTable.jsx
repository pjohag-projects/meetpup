import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';

import { dogparkSelector } from './react_slices/dogpark';
import { addTimeslots, updateTimeslot, timeslotsSelector } from './react_slices/timeslots';

import localizeDatetimeString from './util';

function transformJSONTimeslot(timeslot, timezone) {
  const start = localizeDatetimeString(timeslot.start, timezone);
  const end = localizeDatetimeString(timeslot.end, timezone);
  return {
    id: timeslot.id,
    start: start.substr(start.length - 5),
    end: end.substr(end.length - 5),
    namedDogs: timeslot.named_dogs,
    anonymousDogs: timeslot.anonymous_dogs,
  };
}

function AddDogForm(props) {
  const { id, endpoint } = props;
  const { timezone } = useSelector(dogparkSelector);
  const dispatch = useDispatch();
  const URL = `${endpoint}/${id}/dogs`;
  const [dog, setDog] = useState('');

  const handleChange = (event) => {
    setDog(event.target.value);
  };

  const addDog = async (event) => {
    event.preventDefault();
    const payload = {
      named_dogs: dog ? [dog] : [],
      anonymous_dogs: dog ? 0 : 1,
    };

    const resp = await fetch(URL, {
      method: 'PATCH',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
    });
    let patchedSlot = await resp.json();
    patchedSlot = transformJSONTimeslot(patchedSlot, timezone);
    dispatch(updateTimeslot(patchedSlot));
  };

  return (
    <form className="add-dog-form" onSubmit={addDog}>
      <div>
        <input name="dog-name" placeholder="Dog Name" onChange={handleChange} />
        <button type="submit">Add</button>
      </div>
    </form>
  );
}

AddDogForm.propTypes = {
  id: PropTypes.string.isRequired,
  endpoint: PropTypes.string.isRequired,
};

function TimeSlotRow(props) {
  const {
    endpoint, id, start, end, namedDogs, anonymousDogs, maxDogsPerSlot,
  } = props;

  const numberOfDogs = anonymousDogs + namedDogs.length;
  let dogsText = '';
  const namedDogsText = namedDogs.join(', ');
  const anonymousDogsText = (anonymousDogs > 0) ? `${anonymousDogs} anonymous` : '';
  if (namedDogsText && anonymousDogsText) {
    dogsText = `${namedDogsText} and ${anonymousDogsText}`;
  } else if (namedDogsText) {
    dogsText = namedDogsText;
  } else if (anonymousDogsText) {
    dogsText = anonymousDogsText;
  }

  return (
    <tr>
      <td>{start}</td>
      <td>{end}</td>
      <td>
        <b>
          {`${numberOfDogs}/${maxDogsPerSlot}:`}
        </b>
        {' '}
        {dogsText}
      </td>
      <td>
        {(numberOfDogs < maxDogsPerSlot) ? <AddDogForm id={id} endpoint={endpoint} /> : null}
      </td>
    </tr>
  );
}

TimeSlotRow.propTypes = {
  id: PropTypes.string.isRequired,
  endpoint: PropTypes.string.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  namedDogs: PropTypes.arrayOf(PropTypes.string).isRequired,
  anonymousDogs: PropTypes.number.isRequired,
  maxDogsPerSlot: PropTypes.number.isRequired,
};

export default function TimeSlotsTable(props) {
  const { endpoint } = props;
  const timeslots = useSelector(timeslotsSelector);
  const { maxDogsPerSlot, timezone } = useSelector(dogparkSelector);
  const dispatch = useDispatch();

  useEffect(async () => {
    const response = await fetch(endpoint);
    const fetchedSlots = await response.json();
    const slotsToDisplay = fetchedSlots.map((slot) => transformJSONTimeslot(slot, timezone));
    dispatch(addTimeslots(slotsToDisplay));
  }, []);

  if (timeslots) {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>From</th>
            <th>To</th>
            <th>Dogs</th>
            <th>Add Your Dog</th>
          </tr>
        </thead>

        <tbody>
          {Object.values(timeslots).map((slot) => {
            const rowProps = {
              ...slot,
              key: slot.id,
              endpoint,
              maxDogsPerSlot,
            };
            return (<TimeSlotRow {...rowProps} />);
          })}
        </tbody>
      </table>
    );
  }
  return 'No Timeslots found!';
}

TimeSlotsTable.propTypes = {
  endpoint: PropTypes.string.isRequired,
};
