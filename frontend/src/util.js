/**
 * Localize given datetime with given timezone, return string
 * @param {string} datetime like "2021-03-08T08:00+02:00"
 * @param {string} timezone like "Europe/Helsinki"
 */
export default function localizeDatetimeString(datetime, timezone) {
  return new Date(datetime).toLocaleString('en-GB', { timeZone: timezone, timeStyle: 'short' });
}
