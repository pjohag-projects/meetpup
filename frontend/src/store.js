import { configureStore } from '@reduxjs/toolkit';
import dogparkReducer from './react_slices/dogpark';
import timeslotsReducer from './react_slices/timeslots';

export default configureStore({
  reducer: {
    dogpark: dogparkReducer,
    timeslots: timeslotsReducer,
  },
});
