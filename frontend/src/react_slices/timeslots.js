/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';

export const timeslotsSlice = createSlice({
  name: 'timeslots',
  initialState: {},
  reducers: {
    addTimeslots: (state, action) => {
      action.payload.forEach((timeslot) => {
        state[timeslot.id] = timeslot;
      });
    },
    updateTimeslot: (state, action) => {
      const timeslot = action.payload;
      state[timeslot.id] = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { addTimeslots, updateTimeslot } = timeslotsSlice.actions;

export const timeslotsSelector = (state) => state.timeslots;

export default timeslotsSlice.reducer;
