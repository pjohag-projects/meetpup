/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit';

export const dogparkSlice = createSlice({
  name: 'dogpark',
  initialState: {},
  reducers: {
    setDogpark: (state, action) => {
      state.address = action.payload.address;
      state.closingTime = action.payload.closing_time;
      state.maxDogsPerSlot = action.payload.max_dogs_per_slot;
      state.name = action.payload.name;
      state.openingTime = action.payload.opening_time;
      state.timezone = action.payload.timezone;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setDogpark } = dogparkSlice.actions;

export const dogparkSelector = (state) => state.dogpark;

export default dogparkSlice.reducer;
