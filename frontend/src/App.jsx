import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { setDogpark, dogparkSelector } from './react_slices/dogpark';

import TimeSlotsTable from './TimeSlotsTable';

const SERVER_URL = `${window.location.href}api/v1/`;
const ENDPOINTS = {
  DOGPARK: `${SERVER_URL}dogpark`,
  TIMESLOTS: `${SERVER_URL}time_slots`,
};

export default function App() {
  const dogpark = useSelector(dogparkSelector);
  const dispatch = useDispatch();

  useEffect(async () => {
    const response = await fetch(ENDPOINTS.DOGPARK);
    const dogparkInfo = await response.json();
    dispatch(setDogpark(dogparkInfo));
  }, []);

  if (dogpark) {
    const { openingTime, closingTime } = dogpark;
    const tableProps = {
      endpoint: ENDPOINTS.TIMESLOTS,
    };

    return (
      <>
        <h1>{dogpark.name}</h1>
        <h2>{dogpark.address}</h2>
        <h2>
          Opens
          {' '}
          {openingTime}
          {' '}
          Closes
          {' '}
          {closingTime}
        </h2>
        <TimeSlotsTable {...tableProps} />
      </>
    );
  }
  return 'No Dogpark Information found!';
}

App.propTypes = {
  endpoint: PropTypes.string.isRequired,
};
