# MeetPup (Work In Progress)
Website for dog park meetups, inspired by [Episode 312](https://stackoverflow.blog/2021/02/12/podcast-312-were-building-a-web-app-got-any-advice/) of the StackOverflow podcast.
Mainly, however, I wanted to do this project to learn Flask and MongoDB.

__Note:__ This app was developed on Ubuntu 20.04 with Python version 3.8.

# Run with Docker
Simply run `docker-compose up`. The website will be available on localhost:8080, where you can add dogs to the timeslots.



# Run Manually
To run MeetPup locally, first download and `cd` to this repository.

The app uses a default secret key, which is good enough for local testing, but _must_ be replaced for deployment.
Additionally, the app uses MongoDB for data persistence. By default it will try to connect to a local instance at
`mongodb://localhost:27017`, but you can specify another URI as well.
If you want to do so, create a `config.py` file in the repository to specify the values:
```python
SECRET_KEY = "SOME_LONG_RANDOM_STRING"
DATABASE_URI = "mongodb://172.17.0.2:27017"
```

With either the default or individual secret key and database URI, you can install and start the app as follows:
```bash
# install backend requirements
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt

# build frontend
yarn --cwd frontend/ install
yarn --cwd frontend/ build

# add data
python3 add_to_db.py dogpark timeslots

FLASK_ENV="production"
gunicorn -b 0.0.0.0:8080 "app:create_app()"
```
