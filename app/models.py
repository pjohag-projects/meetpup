from pymongo.collection import ReturnDocument
from app.db import get_db

import pytz
from bson.codec_options import CodecOptions
from bson.objectid import ObjectId


class DogparkInfo:
    def __init__(self) -> None:
        db = get_db()
        info = db.dogpark_info.find_one()
        self.name = info["name"]
        self.address = info["address"]
        self.opening_time = info["opening_time"]
        self.closing_time = info["closing_time"]
        self.timezone = info["timezone"]
        self.max_dogs_per_slot = info["max_dogs_per_slot"]

    def toJSON(self):
        return {
            "name": self.name,
            "address": self.address,
            "opening_time": self.opening_time,
            "closing_time": self.closing_time,
            "timezone": self.timezone,
            "max_dogs_per_slot": self.max_dogs_per_slot,
        }


class TimeSlot:
    def __init__(self, id, start, end, anonymous_dogs, named_dogs) -> None:
        self.id = id
        self.start = start
        self.end = end
        self.anonymous_dogs = anonymous_dogs
        self.named_dogs = named_dogs

    def toJSON(self):
        return {
            "id": str(self.id),
            "start": self.start.isoformat(timespec="minutes"),
            "end": self.end.isoformat(timespec="minutes"),
            "anonymous_dogs": self.anonymous_dogs,
            "named_dogs": self.named_dogs,
        }

    @classmethod
    def byID(cls, id: str):
        db = get_db()
        dogpark_info = DogparkInfo()  # TODO: timezone stuff is duplicate, fix

        aware_times = db.time_slots.with_options(
            codec_options=CodecOptions(
                tz_aware=True, tzinfo=pytz.timezone(dogpark_info.timezone)
            )
        )

        t = aware_times.find_one(ObjectId(id))
        if t:
            return cls(
                t["_id"], t["start"], t["end"], t["anonymous_dogs"], t["named_dogs"]
            )
        else:
            return None

    @classmethod
    def all(cls):
        db = get_db()
        dogpark_info = DogparkInfo()

        aware_times = db.time_slots.with_options(
            codec_options=CodecOptions(
                tz_aware=True, tzinfo=pytz.timezone(dogpark_info.timezone)
            )
        )

        time_slots = list(aware_times.find())
        return [
            cls(t["_id"], t["start"], t["end"], t["anonymous_dogs"], t["named_dogs"])
            for t in time_slots
        ]

    @classmethod
    def add_dogs(cls, id, named_dogs=(), anonymous_dogs=0):
        # TODO: check max dog count
        db = get_db()

        updated = db.time_slots.find_one_and_update(
            {"_id": ObjectId(id)},
            {
                "$addToSet": {"named_dogs": {"$each": named_dogs}},
                "$inc": {"anonymous_dogs": anonymous_dogs},
            },
            return_document=ReturnDocument.AFTER,
        )
        return cls(
            updated["_id"],
            updated["start"],
            updated["end"],
            updated["anonymous_dogs"],
            updated["named_dogs"],
        )
