from flask import Blueprint, jsonify, request
from app.constants import REST_API_PATH
from app.models import DogparkInfo, TimeSlot


rest_api_blueprint = Blueprint("rest_api", __name__, url_prefix=REST_API_PATH)


@rest_api_blueprint.route("/dogpark")
def dogparks_endpoint():
    dogpark_info = DogparkInfo().toJSON()
    return jsonify(dogpark_info)


@rest_api_blueprint.route("/time_slots")
def time_slots_endpoint():
    time_slots = TimeSlot.all()
    time_slots = [t.toJSON() for t in time_slots]
    return jsonify(time_slots)


@rest_api_blueprint.route("/time_slots/<id>")
def time_slots_endpoint_s(id: str):
    t = TimeSlot.byID(id)
    if t:
        return jsonify(t.toJSON())
    else:
        return (f"No Time Slot with ID {id}", 404)


@rest_api_blueprint.route("/time_slots/<id>/dogs", methods=("PATCH",))
def time_slots_dogs_endpoint(id: str):
    body = request.json
    named_dogs = body.get("named_dogs", [])
    anonymous_dogs = body.get("anonymous_dogs", 0)
    updated_timeslot = TimeSlot.add_dogs(id, named_dogs, anonymous_dogs)
    return jsonify(updated_timeslot.toJSON())