import os

from flask import Flask
from app.db import close_db
from app.rest_api import rest_api_blueprint
from app.views import website_bp


def create_app():
    # create and configure the app
    app = Flask(__name__)

    DEFAULT_KEY = "CHANGE_ME"
    app.config.from_mapping(
        DATABASE_URI="mongodb://localhost:27017",
        SECRET_KEY=DEFAULT_KEY,
    )
    app.config.from_pyfile("../config.py", silent=True)
    if app.config["ENV"] == "production" and app.config["SECRET_KEY"] == DEFAULT_KEY:
        # print colored warning
        print("\033[93m" + "WARNING: Using default secret key!" + "\033[0m")

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except FileExistsError:
        pass
    app.register_blueprint(rest_api_blueprint)
    app.register_blueprint(website_bp)
    app.teardown_appcontext(close_db)

    return app
