from flask import current_app, g
from pymongo import MongoClient


def get_db():
    if "db" not in g:
        client = MongoClient(current_app.config["DATABASE_URI"])
        g.db = client.meetpup_db
    return g.db


def close_db(_=None):
    db = g.pop("db", None)
    if db:
        db.client.close()