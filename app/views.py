from flask import Blueprint, render_template

website_bp = Blueprint("website", __name__)


@website_bp.route("/")
def website_view():
    return render_template("base.html")
