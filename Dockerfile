FROM node:14.16.0-alpine3.13 as frontend-builder

# build frontend
WORKDIR /frontend

COPY frontend/package.json frontend/yarn.lock ./
RUN yarn install

COPY frontend/.babelrc frontend/.eslintrc.js frontend/webpack.config.js ./ 
COPY frontend/src ./src
RUN yarn build


FROM python:3.9.2

EXPOSE 8080

# set non-root user
RUN groupadd -r meetpup && \
    useradd --no-log-init -r -m -g meetpup meetpup
ENV PATH="/home/meetpup/.local/bin:$PATH"
WORKDIR /home/meetpup/meetpup-backend
RUN chown -R meetpup:meetpup .
USER meetpup

# Setup configuration
COPY dogpark_config.json add_to_db.py start.sh ./
RUN echo "DATABASE_URI = 'mongodb://meetpup-mongo:27017'" > /home/meetpup/meetpup-backend/config.py

# Setup backend and frontend
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY ./app ./app
COPY --from=frontend-builder /app/static/dist/bundle.js ./app/static/dist/bundle.js

CMD  "./start.sh"
