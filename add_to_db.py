"""Script to add dogpark information and daily timeslots to database"""

import sys

import json
from datetime import datetime, timedelta
import time
import pytz

from pymongo.database import Database
from pymongo import MongoClient


# Get database URI
try:
    from importlib.util import spec_from_file_location, module_from_spec
    from pathlib import Path

    path_to_config = Path(__file__).parent.joinpath("config.py").absolute()
    spec = spec_from_file_location("config", str(path_to_config))
    config = module_from_spec(spec)
    spec.loader.exec_module(config)
    from config import DATABASE_URI
except KeyError:
    DATABASE_URI = "mongodb://localhost:27017"


class InvalidConfigException(Exception):
    pass


def check_config(dogpark_config):
    """Raises InvalidConfigException if given config is invalid"""
    exception_message = ""
    timezone = dogpark_config["timezone"]
    opening_minutes = dogpark_config["opening_time"][-2:]
    closing_minutes = dogpark_config["closing_time"][-2:]

    if timezone not in pytz.all_timezones:
        exception_message += f"Unrecognized timezone {timezone}\n"
    if opening_minutes != "00" and opening_minutes != "30":
        exception_message += (
            f"Opening time must be full or half hour. Got {opening_minutes}\n"
        )
    if closing_minutes != "00" and closing_minutes != "30":
        exception_message += (
            f"Closing time must be full or half hour. Got {closing_minutes}\n"
        )

    if exception_message != "":
        raise InvalidConfigException(exception_message)


def get_dogpark_datetime(local_datetime: datetime, time_string: str):
    dogpark_dt = time.strptime(time_string, "%H:%M")
    dogpark_dt = local_datetime.replace(
        hour=dogpark_dt.tm_hour,
        minute=dogpark_dt.tm_min,
        second=0,
        microsecond=0,
    )
    return dogpark_dt.astimezone(pytz.utc)


def add_today_timeslots(dogpark_config, db: Database):
    local_timezone = pytz.timezone(dogpark_config["timezone"])
    local_datetime = datetime.now(local_timezone)

    opening_time = get_dogpark_datetime(local_datetime, dogpark_config["opening_time"])
    closing_time = get_dogpark_datetime(local_datetime, dogpark_config["closing_time"])

    time_slots = []

    current_start = opening_time.replace()
    current_end = current_start + timedelta(minutes=30)

    while current_end <= closing_time:
        time_slots.append(
            {
                "start": current_start,
                "end": current_end,
                "anonymous_dogs": 0,
                "named_dogs": [],
            }
        )
        current_start = current_end
        current_end = current_end + timedelta(minutes=30)

    db.time_slots.delete_many({})
    db.time_slots.insert_many(time_slots)


def add_dogpark_info(dogpark_config, db: Database):
    db.dogpark_info.delete_many({})
    db.dogpark_info.insert_one(dogpark_config)


if __name__ == "__main__":
    with open("dogpark_config.json") as file:
        dogpark_config = json.load(file)

    check_config(dogpark_config)

    client = MongoClient(DATABASE_URI, serverSelectionTimeoutMS=2000)
    db = client.meetpup_db

    if "dogpark" in sys.argv:
        add_dogpark_info(dogpark_config, db)
    if "timeslots" in sys.argv:
        add_today_timeslots(dogpark_config, db)

    client.close()
